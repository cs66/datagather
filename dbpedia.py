from SPARQLWrapper import SPARQLWrapper, JSON
sp = SPARQLWrapper('http://dbpedia.org/sparql')
sp.setQuery("""
select ?movie
 where {
  ?movie rdf:type schema:Movie
}
LIMIT 1000
""")
sp.setReturnFormat(JSON)
res = sp.query().convert()
for r in res["results"]["bindings"]:
  s = "%s" % (r["movie"]["value"])
  print s.encode('utf-8')
