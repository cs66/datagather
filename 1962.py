from SPARQLWrapper import SPARQLWrapper, JSON
sp = SPARQLWrapper('https://query.wikidata.org/bigdata/namespace/wdq/sparql')
sp.setQuery("""
SELECT DISTINCT ?movie ?movieLabel ?date ?title
WHERE
{
    ?movie wdt:P31 wd:Q11424 .
    ?movie wdt:P577 ?date .
    ?movie wdt:P1476 ?title
    FILTER(?date > "1962-01-01T00:00:00Z"^^xsd:dateTime)
    FILTER(?date < "1963-01-01T00:00:00Z"^^xsd:dateTime)
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
}
ORDER BY ?movie
LIMIT 1000
""")
sp.setReturnFormat(JSON)
res = sp.query().convert()
for r in res["results"]["bindings"]:
  print "%s	%s	%s	%s" % (r["movie"]["value"], r["date"]["value"], r["movieLabel"]["value"], r["title"]["value"])
