from SPARQLWrapper import SPARQLWrapper, JSON
sp = SPARQLWrapper('https://query.wikidata.org/bigdata/namespace/wdq/sparql')
sp.setQuery("""
SELECT ?country ?countryLabel ?continentLabel ?capitalLabel ?flag ?tld ?pop ?area ?gdp
WHERE
{
    ?country wdt:P31 wd:Q160016 .
    ?country wdt:P30 ?continent .
    ?country wdt:P36 ?capital .
    ?country wdt:P41 ?flag .
    ?country wdt:P78 ?tld .
    OPTIONAL {?country wdt:P2131 ?gdp} .
    OPTIONAL {?country wdt:P1082 ?pop} .
    OPTIONAL {?country wdt:P2046 ?area} .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
}
ORDER BY ?countryLabel
LIMIT 400
""")
print "-- one"
sp.setReturnFormat(JSON)
print "-- two"
res = sp.query().convert()
print "-- three"
cl = []
for r in res["results"]["bindings"]:
  def get(f):
    if f in r:
      return r[f]['value']
    else:
      return 'null'
  cl.append(map(get, 'countryLabel,continentLabel,capitalLabel,area,pop,gdp,flag,tld'.split(',')))
print """
CREATE TABLE world
(name VARCHAR(50) PRIMARY KEY
,continent VARCHAR(50)
,capital VARCHAR(50)
,area INT
.population INT
,gdp INT
,flag VARCHAR(100)
,tld VARCHAR(3)
);
"""
prev = None
for c in cl:
  pre = ""
  if c[0]==prev:
    pre = "-- "
  def fmt(s):
    if s=='null':
      return 'null'
    else:
      return "'" + s.replace("'","''") + "'"
  s = pre+"INSERT INTO world VALUES (%s,%s,%s,%s,%s,%s,%s,%s);" %\
        tuple(map(fmt, c))
  print s.encode('utf-8')

